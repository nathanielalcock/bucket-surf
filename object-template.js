export default (proto, hostname, filename, sourcetime, localtime) => `
        <tr>
          <td>
            <a href='${proto}${hostname}/${filename}'>
              ${filename}
            </a>
          </td>
          <td>
            <time datetime='${sourcetime}'>
              ${localtime.toLocaleString()}
            </time>
          </td>
        </tr>
`;
