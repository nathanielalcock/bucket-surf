# Bucket Surf

Object storage has been popularized by Amazon Simple Storage Service.
Unlike old-school FTP, it lacks a compelling web frontend.
Bucket surf adds that missing component.

# Dependencies

* NodeJS
* Express web framework
* xml-js to read the S3 bucket listings
