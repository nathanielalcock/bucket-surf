export default (title, links) => `
<!DOCTYPE html>
<html lang='en'>
<head>
  <meta charset='utf-8' />
  <meta name='viewport' content='width=device-width,
                                 initial-scale=1.0,
                                 user-scalable=yes' />
  <title>${title}</title>
  <link rel='stylesheet' href='style.css'>
  <script src='index.js'></script>
</head>
<body>
  <header>
    <h1><img width='50px' height='50px' src='bucket.png' alt='bucket'>${title}</h1>
    <form action='/' method='GET'>
      <input type='text' name='hostname' placeholder='S3 hostname'/> 
      <input type='submit' value='Submit' /> 
    </form>
  </header>
  <main>
    <table id='listing'>
      <thead>
        <th>object</th>
        <th>last modified</th>
      </thead>
      <tbody>
        ${links}
      </tbody>
    </table>
  </main>
</body>
`;
