import convert           from 'xml-js';
import express           from 'express';
import https             from 'https';
import objectTemplate    from './object-template.js';
import pageTemplate      from './page-template.js';
import path              from 'path';
import { fileURLToPath } from 'url';

// Express setup
const app        = express();
const port       = process.env.PORT || 3000;
const __filename = fileURLToPath(import.meta.url);
const __dirname  = path.dirname(__filename);

app.get('/', (request, response) => {
  if (typeof(request.query.hostname) !== 'undefined') {
    // Configure HTTP request
    const mkOptions = (
      hostname,
      port   = 443,
      path   = '/',
      method = 'GET'
    ) => {
      return {hostname, port, path, method}
    };

    const options = mkOptions(
      request.query.hostname,
      request.query.port,
      request.query.path,
      request.query.method
    );

    const innerRequest = https.request(options, innerResponse => {
      innerResponse.on('data', data => {
        // Convert XML to JSON
        const converted = convert.xml2js(data, {compact: true});
        // Build the page of links
        const links = converted.ListBucketResult.Contents.map(each => {
          const filename = each.Key._text;
          const localtime = new Date(each.LastModified._text);
          return objectTemplate(
                   options.proto,
                   options.hostname,
                   filename, 
                   each.LastModified._text,
                   localtime
                 );
        });
        const title = `
          bucket surf: ${converted.ListBucketResult.Name._text}
        `;
        response.send(pageTemplate(title, links.join('')));
      });
    });

    innerRequest.on('error', error => {
      console.error(error)
    });
    innerRequest.end();
  } else {
    response.send(pageTemplate('bucket surf', ''));
  }
});

app.use(express.static(path.join(__dirname, 'public')))

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

