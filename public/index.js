const suggest = () => {
  document.getElementById('listing').outerHTML = `
    <p id='suggestion'>
      Nothing to show, try submitting an S3-compatible object storage
      hostname in the form above and clicking 'Submit'.
    </p>
  `;
}

document.addEventListener('DOMContentLoaded', () => {
  document.querySelector('#listing td') ? null : suggest();
});
